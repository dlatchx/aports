# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=release-plz
pkgver=0.3.60
pkgrel=0
pkgdesc="Release Rust packages without using the command line"
url="https://github.com/MarcoIeni/release-plz"
license="MIT OR Apache-2.0"
arch="all"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	openssl-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/MarcoIeni/release-plz/archive/release-plz-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

check() {
	RUST_MIN_STACK=8388608 cargo test --frozen --no-default-features -- \
		--skip "next_ver" \
		--skip "input_generates_correct_release_request"
}

build() {
	OPENSSL_NO_VENDOR=1 cargo auditable build -p "$pkgname" --release --frozen
	mkdir -p completions/
	local compgen="target/release/$pkgname generate-completions"
	$compgen bash >"completions/$pkgname"
	$compgen fish >"completions/$pkgname.fish"
	$compgen zsh >"completions/_$pkgname"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE-MIT -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 644 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
3e177bdead19bef4a20d028b60d0ff13305d798a9eb94a835ef92ab17ef6057ac558fc921d6d13b7a3bf4ecc10dfcba4dcd2a0b1fbf2723a95ebd48502bc0d03  release-plz-v0.3.60.tar.gz
"
