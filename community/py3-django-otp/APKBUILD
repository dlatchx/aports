# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-django-otp
_pyname=django_otp
pkgver=1.4.1
pkgrel=1
arch="noarch"
pkgdesc="A pluggable framework for adding two-factor authentication to Django using one-time passwords."
url="https://pypi.python.org/project/django-otp"
license="BSD-2-Clause"
depends="
	py3-django
	py3-qrcode
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	py3-hatchling
	"
checkdepends="
	py3-freezegun
	"
options="!check" # Can't find module for some reason
source="$pkgname-$pkgver.tar.gz::https://pypi.io/packages/source/d/django-otp/django_otp-$pkgver.tar.gz"
builddir="$srcdir"/$_pyname-$pkgver
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	DJANGO_SETTINGS_MODULE="test_project.settings" \
	PYTHONPATH="test" \
	python3 -s -m django test django_otp
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="
bb041a6ecd93dbad4f83d6593dfbc2f4768e1a3e68df332a61ed9ce483a8ec7090179a6865b128d02a2fc5ba2b42cd63dc16595a7b98e2728733fc274d506b1a  py3-django-otp-1.4.1.tar.gz
"
