# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Pablo Correa Gomez <ablocorrea@hotmail.com>
pkgname=libxmlb
pkgver=0.3.18
pkgrel=0
pkgdesc="Library to help create and query binary XML blobs"
url="https://github.com/hughsie/libxmlb"
arch="all"
license="LGPL-2.1-or-later"
makedepends="meson glib-dev gobject-introspection-dev xz-dev zstd-dev"
checkdepends="shared-mime-info"
subpackages="$pkgname-dev:_dev $pkgname-dbg $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/hughsie/libxmlb/archive/refs/tags/$pkgver.tar.gz"

build() {
	abuild-meson \
		-Dgtkdoc=false \
		-Dtests=true \
		-Dstemmer=false \
		-Dintrospection=true \
		output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	rm -rf "$pkgdir"/usr/share/installed-tests
	rm -rf "$pkgdir"/usr/libexec/installed-tests
}

_dev() {
	default_dev

	amove usr/bin/xb-tool
}

sha512sums="
2576aca6b7651cf484694ad157914982ef1e3949ea641b4181966e5d5292b3d8c16318fe61b14d8c7e6d0e0d184ed30d498f874f5ada196f0ca69c6180d9061f  libxmlb-0.3.18.tar.gz
"
