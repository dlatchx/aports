# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-pyqt-builder
pkgver=1.16.0
pkgrel=1
pkgdesc="The PEP 517 compliant PyQt build system"
url="https://www.riverbankcomputing.com/software/pyqt-builder/"
arch="noarch"
license="custom:sip"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://pypi.python.org/packages/source/P/PyQt-builder/PyQt-builder-$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/PyQt-builder-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/PyQt_*.whl
}

sha512sums="
1584b41e0c260a45b2b883d035e7611b29501a24172a8d9f36702c4fbde8c3f7953a6897781b59f48b9fa1ceab51eb3767afa83e0e3ff097caf029d87a7114d8  PyQt-builder-1.16.0.tar.gz
"
