# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=conky
pkgver=1.20.0
pkgrel=0
pkgdesc="Advanced, highly configurable system monitor for X based on torsmo"
url="https://github.com/brndnmtthws/conky"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	alsa-lib-dev
	cairo-dev
	cmake
	curl-dev
	gawk
	glib-dev
	imlib2-dev
	libxdamage-dev
	libxext-dev
	libxft-dev
	libxinerama-dev
	libxml2-dev
	linux-headers
	lua5.4-dev
	ncurses-dev
	pango-dev
	samurai
	tolua++
	wayland-dev
	libxi-dev
	wayland-protocols
	wireless-tools-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/brndnmtthws/conky/archive/v$pkgver.tar.gz"
options="!check"

build() {
	cmake -B build -G Ninja \
		-DRELEASE=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_CURL=ON \
		-DBUILD_XDBE=ON \
		-DBUILD_IMLIB2=ON \
		-DBUILD_RSS=ON \
		-DBUILD_WLAN=ON \
		-DBUILD_I18N=OFF \
		-DBUILD_LUA_CAIRO=ON \
		-DBUILD_WAYLAND=ON \
		-DLUA_LIBRARIES="/usr/lib/lua5.4/liblua.so"
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -D -m644 COPYING $pkgdir/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
ecd6856feacad7d0e8e20a6c6f2807ea591f88fd693c94c76619828fdd178a995c6421bd0c279f4369b2bbc4a51fd7021fcecb4d54daefcf946c053b5ed734f5  conky-1.20.0.tar.gz
"
